export class Programs
{
    _programLevelId: number
    _productId: number

    constructor(programLevelId: number,productId: number) {
        this._programLevelId = programLevelId
        this._productId = productId
    }

    public isMS2() {
        if(this._programLevelId>=1 && this._programLevelId <= 7)
            return true;
        return false
    }
    public isSA1250() {
        if(this._programLevelId==8)
            return true;
        return false
    }

    public isMS3() {
        if(this._programLevelId>=10 && this._programLevelId <= 13)
            return true;
        return false
    }
    
    public isCS1() {
        if(this._programLevelId>=14 && this._programLevelId <= 15)
            return true;
        return false
    }
    public isMS3orCS1() {
        if(this._programLevelId>=10 && this._programLevelId <= 15)
            return true;
        return false
    }

    public isShare() {
        if(this._productId==1)
            return true;
        return false
    }

    public isAdmin() {
        if(this._productId==2)
            return true;
        return false
    }
    public isHiD() {
        if(this._productId==25)
            return true;
        return false
    }

}