import {NextFunction,Request,Response} from 'express'
import { Client } from 'pg' 
import { Programs } from '../helpers/Program'
import {IProductPrice,Idbfactors,IPriceFactors,adjFactors} from "../models/ProductPriceModels"

export class ProductPriceController {
    public getPrice = async (req: Request , res: Response, next: NextFunction) => {
        let qs = <IProductPrice><unknown> req.query
        let factors : IPriceFactors ={ageFactor : 0, tierFactor : 0, maritalFactor : 0, ageAhpFactor : 0, 
            geoFactor : 0,  healthFactor : 0, ms3Admin: 0,baseRate:0,HiDFactor:0 }
        let price = 0;
        let SharePrice=0;
        let prog = new Programs(qs.programLevelId,qs.productId)    

        // To get the MS3/CS1 Admin price or HiD, we must first get the MS3/CS share price
        if (prog.isMS3orCS1() && (prog.isAdmin() || prog.isHiD() )) {  // MS3/CS Medishare admin 

            let PrevProdID=qs.productId;
            qs.productId=1  // Share Portion
            
            await this.GetPricingFactors(qs)
            .then ( (f : IPriceFactors) => {
                SharePrice=this.CalcMS3Share(f)
                qs.productId=PrevProdID;
            })
            .catch((e) => {
                return res.status(401).json(e);
            })
        }
        qs = <IProductPrice><unknown> req.query
        await this.GetPricingFactors(qs)
        .then ( (f : IPriceFactors) => {
           while(1==1) {  // break when we find a solution

                if (prog.isMS2()){
                    if(prog.isShare()) { price=f.baseRate * (1+f.geoFactor/100); break;}
                    if(prog.isAdmin()) { price=f.baseRate; break;}
                    if(prog.isHiD())   { price=SharePrice * f.HiDFactor; break;}                
                }
                if (prog.isMS3orCS1()) {
                    if(prog.isShare()) { price=this.CalcMS3Share(f); break;}                
                    if(prog.isAdmin()) { price=(SharePrice * f.ms3Admin) + Number(f.baseRate); break;}
                    if(prog.isHiD())   { price=SharePrice * f.HiDFactor; break;}
                }    
                        
                price=f.baseRate;                          // Everything else is just a lookup
                break;                
           }
            factors=f;
        })
        .catch((e) => {
            return res.status(401).json(e);
        })
        return res.status(200).json({
            "product_price" : price,
            "factors" : factors
        })
    };
    private CalcMS3Share(f : IPriceFactors):number {
        return f.baseRate * f.ageFactor  * f.tierFactor * f.maritalFactor * f.ageAhpFactor * f.geoFactor * f.healthFactor;
    }
    private async GetPricingFactors(qv : IProductPrice) {
        const factors : IPriceFactors ={ageFactor : 1, tierFactor : 1, maritalFactor : 1, ageAhpFactor : 1, 
            geoFactor : 1,  healthFactor : 1, ms3Admin: 1,baseRate:0, HiDFactor:1 }
        if(qv.programLevelId <=7 && qv.productId ==1)factors.geoFactor=0;  // MS2 uses percentages, not factors

        // NOTE:  Using Injectable version of param for age for now as the $ method puts quotes around the parameter and this 
        //        blows up the query for containment operator on a numeric field

        const SQL = `
            select rate_adjustment_factor_id as factor_id,calculation as factor_value
            from singular_rate_adjustments
            where program_level_id=$1 and product_id=$2 and during @> $3::timestamptz
            and (
                (rate_adjustment_factor_id=`+ adjFactors.Age    + `  and value_numeric_range @> ` + qv.oldestAge + `)
            or (rate_adjustment_factor_id=`+ adjFactors.Tier    + `  and value_numeric = $5 )
            or (rate_adjustment_factor_id=`+ adjFactors.Geo     + `  and value_string = $6 )
            or (rate_adjustment_factor_id=`+ adjFactors.Marital + `  and value_string = $4)
            or (rate_adjustment_factor_id=`+ adjFactors.HiD + ` )
            or (rate_adjustment_factor_id=`+ adjFactors.ms3Admin + ` )    
            or (rate_adjustment_factor_id=`+ adjFactors.AgeAHP  + `  and value_numeric_range @> ` + qv.oldestAge + `)
            or (rate_adjustment_factor_id=`+ adjFactors.MS2AgeTier + `  and value_numeric_range @> ` + qv.oldestAge + ` and value_numeric = $5 )
            )
            union
            select ` + adjFactors.Base + ` as factor_id, base_amount as factor_value
            from product_program_level_availabilities
            where program_level_id=$1 and product_id=$2 and during @> $3::timestamptz
        `;
        const connectString = process.env.DATABASE_URL;                
        const client = new Client({
            connectionString: connectString
        })
        //                   $1               $2            $3           $4        $5     $6         $7
        const parms = [qv.programLevelId,qv.productId,qv.timeAt,qv.maritalStatus,qv.tier,qv.state]//,qv.oldestAge] //;
        await client.connect()
        await client.query(SQL,parms)
        .then( (res) => {
            res.rows.forEach( (r : Idbfactors) =>{   // Pivot
                if(r.factor_value != 0.0)
                {
                    if(r.factor_id == adjFactors.Age        ) factors.ageFactor     = r.factor_value;
                    if(r.factor_id == adjFactors.Tier       ) factors.tierFactor    = r.factor_value;
                    if(r.factor_id == adjFactors.Geo        ) factors.geoFactor     = r.factor_value;   
                    if(r.factor_id == adjFactors.Marital    ) factors.maritalFactor = r.factor_value;
                    if(r.factor_id == adjFactors.AgeAHP     ) factors.ageAhpFactor  = r.factor_value;
                    if(r.factor_id == adjFactors.ms3Admin   ) factors.ms3Admin      = r.factor_value;
                    if(r.factor_id == adjFactors.HiD        ) factors.HiDFactor     = r.factor_value;
                    if(r.factor_id == adjFactors.Base       ) factors.baseRate      = r.factor_value;
                    if(r.factor_id == adjFactors.MS2AgeTier ) factors.baseRate      = r.factor_value;
                }
            })
        })
        .catch( (err) => {
            console.log(err)
            return Promise.reject("Query Failed")
        })
        return factors;
    } 
}