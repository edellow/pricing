import { Request, Response } from 'express';
import { Product, ProductInterface } from '../models/product.model'
import { UpdateOptions, DestroyOptions } from 'sequelize'

export class ProductsController{

    public index (_req: Request, res: Response) {
        Product.findAll<Product>({})
            .then((Products : Array<Product>) => res.json(Products))
            .catch((err : Error) => res.status(500).json(err))
    }

    public create (req: Request, res: Response) {
        const params : ProductInterface = req.body

        Product.create<Product>(params)
            .then((Product : Product) => res.status(201).json(Product))
            .catch((err : Error) => res.status(500).json(err))
    }

    public show (req: Request, res: Response) {
        const ProductId : number = parseInt( req.params.id,10)

        Product.findByPk<Product>(ProductId)
            .then((Product : Product|null) => {
                if (Product) {
                    res.json(Product)
                } else {
                    res.status(404).json({errors: ['Product not found']})
                }
            })
            .catch((err : Error) => res.status(500).json(err))
    }

    public update (req: Request, res: Response) {
        const ProductId : number = parseInt( req.params.id,10)
        const params : ProductInterface = req.body

        const options : UpdateOptions = {
             where: {id: ProductId},
             limit: 1
        }

        Product.update(params, options)
            .then(() => res.status(202).json({data: "success"}))
            .catch((err : Error) => res.status(500).json(err))
    }

    public delete (req: Request, res: Response) {
        const ProductId : number = parseInt( req.params.id,10)
        const options : DestroyOptions = {
            where: {id: ProductId},
            limit: 1
       }

        Product.destroy(options)
            .then(() => res.status(204).json({data: "success"}))
            .catch((err : Error) => res.status(500).json(err))
    }
}
