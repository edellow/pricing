import { Router } from "express";
import { ProductPriceController } from "../controllers/product-price.controller"

// Sequilize
import { ProductsController }     from "../controllers/products.controller"

export class Routes {
    public productPriceController: ProductPriceController = new ProductPriceController();
    public productsController:     ProductsController     = new ProductsController();

    public routes(router : Router): void {
      router.get('/product-price', this.productPriceController.getPrice);
       
      router
        .route("/products")
        .get(this.productsController.index)
        .post(this.productsController.create);
  
      router
        .route("/products/:id")
        .get(this.productsController.show)
        .put(this.productsController.update)
        .delete(this.productsController.delete);
  }
}