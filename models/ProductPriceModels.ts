

// the values from the QueryString
export type IProductPrice = {
    productId : number,
    oldestAge : number,
    maritalStatus : String,
    state : String,
    zipCode : String, 
    operationID : String,
    timeAt : String,
    tier : number,
    programLevelId: number
};


// The psudo columns returned from the query
export type Idbfactors = {
    factor_id : number,
    factor_value : number
}

// factors from the database used to calculate product prices
export type IPriceFactors = {
    ageFactor : number,
    tierFactor : number,
    maritalFactor : number,
    ageAhpFactor : number,
    geoFactor : number,
    healthFactor : number,
    ms3Admin : number,
    HiDFactor : number,
    baseRate : number
}

   // The IDs of factors as defined in rate_adjustment_factors in the Postgres database
   export const adjFactors = {
    Age: 1,
    Tier: 2,
    Geo:  4,
    MS2AgeTier : 5,
    Marital: 7,
    ms3Admin: 8,
    HiD: 9,
    AgeAHP: 10,
    Base : -1
}