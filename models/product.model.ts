import { Sequelize,Model, DataTypes } from "sequelize";
import { database } from "../config/database";

export class Product extends Model {
  public id!: number; // Note that the `null assertion` `!` is required in strict mode.
  public name!: string;
  public description!: string;
  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export interface ProductInterface {
  name: string;
}

Product.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: new DataTypes.STRING(128),
      allowNull: false
    },
    description: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    createdAt: {
        type: new DataTypes.DATE,
        field: 'created_at',
      },
      updatedAt: {
        type: new DataTypes.DATE,
        field: 'updated_at'
      }
  },
  {
    // underscored: true,
    tableName: "products",
    sequelize: database // this bit is important
  }
);

//Node.sync({ force: true }).then(() => console.log("table created"));
