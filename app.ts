import * as express from "express";
import { Routes } from "./routes/routes";
require('dotenv').config();

class App {
  public app: express.Application;
  public router = express.Router();
  public routePrv: Routes = new Routes();

  constructor() {
    this.app = express();
    this.config();
    console.log("DatabaseULR=" + process.env.DATABASE_URL)

    this.routePrv.routes(this.router);
    this.app.use("/api/v1/",this.router)
  }

  private config(): void {
    // support application/json type post data
    this.app.use(express.json());
    
    //support application/x-www-form-urlencoded post data
    this.app.use(express.urlencoded({ extended: false }));
  }
}

export default new App().app;
