"use strict";
exports.__esModule = true;
exports.database = void 0;
var sequelize_1 = require("sequelize");
require('dotenv').config();
var cs = process.env.DATABASE_URL;
exports.database = new sequelize_1.Sequelize(cs, {
    dialect: 'postgres'
});
