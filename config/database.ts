import {Sequelize} from 'sequelize';
require('dotenv').config();

const cs : string = process.env.DATABASE_URL

export const database = new Sequelize( cs,
    { 
        dialect: 'postgres'
    }
);