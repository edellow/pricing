npm install -g typescript nodemon ts-node prettier body-parser
npm install @types/express @types/dotenv @types/body-parser
tsc --init

##Create .env file
DATABASE_URL=postgres://postgres:{password}@127.0.0.1:5432/ccm_core_development

##one-time migration of data
npm run seed

##run project
npm start
